#!/bin/bash

source_file="square_progessive_curved_base.svg"
output_file="square_progessive_curved_base_$(date +%Y%m%d_%H%M%S).svg"

while read p; do
  #echo $p | grep -qE "(\s|^)style="
  #if [ $? = 0 ]; then
    style_line=$p
    grey_color=$(echo $style_line | grep -o -E "fill:#([a-f0-9]){2}" | awk -F# '{print $2}')
    if [ ! -z $grey_color ]; then
      if [ $grey_color != "00" ]; then
        dec_grey=$(echo "obase=10; ibase=16; ${grey_color^^}" | bc)
        ngrey=$(echo "((((255 - $dec_grey) / 5) * 5 * 2))" | bc)
        ngrey=$(echo "$ngrey / 1" | bc)
        
  #       if [ $ngrey -gt 255 ]; then
  #         ngrey=255
  #       fi
        newgrey=$((255 - ngrey))
        #echo $dec_grey $ngrey $newgrey
       # newgrey=$dec_grey
        if [ ${#newgrey} = 1 ]; then newgrey="0$newgrey"; fi
        ngreyhex=$(echo "obase=16; $newgrey" | bc | tr '[:upper:]' '[:lower:]')
        nstyle_line=$(echo $style_line | sed "s/${grey_color}${grey_color}${grey_color}/${ngreyhex}${ngreyhex}${ngreyhex}/g")
  #       echo "OLD : $style_line"
  #       echo "NEW : $nstyle_line"
        echo "$nstyle_line" >>${output_file}
        # echo "$style_line" >>${output_file}
      else
         echo "$style_line" >>${output_file}
      fi
    else
      echo "$style_line" >>${output_file}
    fi
  #else
  #  echo "$p" >>${output_file}
  #fi

done <$source_file
